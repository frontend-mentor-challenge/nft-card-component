# Frontend Mentor - NFT preview card component solution

This is a solution to the [NFT preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

### Screenshot

![Desktop](./screenshots/desktop.jpg)
![Mobile](./screenshots/mobile.jpg)
### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/nft-card-component)
- Live Site URL: [Live](https://nft-card-component.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
### What I learned

- I have learned to use the before and after pseudo elements to position the icons.
- I also learned how to use pseudo classes for active states.

### Continued development

### Useful resources
- [Figma](https://www.figma.com/) - For design
## Author

- Website - [Issac Leyva](https://issacleyva.com) - In Construction
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)
## Acknowledgments
